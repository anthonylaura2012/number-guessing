I chose the number guessing game because I wanted to have a web app and demonstrate React Hooks,
 but I also wanted it to be very simple.

One problem was around testing of the high scores.
When I first added high scores, I added an entry every time a guess was entered.
Then I wrote a test to say that the entry would NOT be added if it was an incorrect guess.
However, that test always passed, regardless of what I did to it.
So I ended up just taking that test out and verifying manually.
