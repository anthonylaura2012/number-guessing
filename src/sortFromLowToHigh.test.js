import { sortFromLowToHigh } from "./sortFromLowToHigh";

test('sort array of one high score', () => {
  const highScores = [{
    name: 'Anthony',
    numTries: 42
  }];
  const sorted = sortFromLowToHigh(highScores);
  const first = sorted[0];
  expect(first.numTries).toEqual(42);
});

test('sort array of two high scores', () => {
  const highScores = [
    {
      name: "Anthony",
      numTries: 42
    },
    {
      name: "Anthony",
      numTries: 1
    }];
  const sorted = sortFromLowToHigh(highScores);
  const first = sorted[0];
  expect(first.numTries).toEqual(1);
});
