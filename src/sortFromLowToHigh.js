export const sortFromLowToHigh = (highScores) => {
  highScores.sort((a, b) => a.numTries - b.numTries);
  return highScores;
};
