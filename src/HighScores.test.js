import { render, screen } from '@testing-library/react';
import { HighScores } from "./HighScores";

test('renders High Scores', () => {
  render(<HighScores />);

  expect(screen.getByText('High Scores')).toBeInTheDocument();
});

test('renders numTries of 1', () => {
  const highScore = {
    name: 'Anthony',
    numTries: 1
  };
  render(<HighScores highScores={[highScore]} />);

  expect(screen.getByText('Anthony: 1')).toBeInTheDocument();
});

test('renders numTries of 42', () => {
  const highScore = {
    name: 'Anthony',
    numTries: 42
  };
  render(<HighScores highScores={[highScore]} />);

  expect(screen.getByText('Anthony: 42')).toBeInTheDocument();
});

test('renders name of Billy', () => {
  const highScore = {
    name: 'Billy',
    numTries: 42
  };
  render(<HighScores highScores={[highScore]} />);

  expect(screen.getByText('Billy: 42')).toBeInTheDocument();
});
