import React from "react";

export function HighScores ({ highScores }) {
  const downloadFile = async () => {
    const json = JSON.stringify(highScores);
    const blob = new Blob([json],{type:'application/json'});
    const href = await URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = href;
    link.download = "highscores.json";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <>
      <h3>High Scores</h3>
      <ul>
        {highScores && highScores.map((x, index) =>
          <li key={x.name + x.numTries + '_' + index}>
            {x.name}: {x.numTries}
          </li>
        )}
      </ul>
      <button onClick={downloadFile}>Export</button>
    </>
  );
}
