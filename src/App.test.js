import { render, screen } from '@testing-library/react';

import App from './App';
import userEvent from "@testing-library/user-event";

test('renders initial message', () => {
  render(<App />);
  const initialMessage = screen.getByText(/Pick a number between 1 and 10/i);
  expect(initialMessage).toBeInTheDocument();
});

test('message starts empty', () => {
  render(<App />);

  const successMessage = screen.queryByText('Correct!');
  expect(successMessage).not.toBeInTheDocument();

  const failureMessage = screen.queryByText('Try again :(');
  expect(failureMessage).not.toBeInTheDocument();
});

test('renders High Scores', () => {
  render(<App />);

  expect(screen.getByText('High Scores')).toBeInTheDocument();
});

test('renders high score of 1 attempt', () => {
  render(<App />);

  userEvent.type(screen.getByLabelText('Your name'), 'Billy');

  userEvent.type(screen.getByLabelText('Guess'), '3');
  userEvent.click(screen.getByText('Submit'));

  expect(screen.getByText(/Correct!/i)).toBeInTheDocument();

  expect(screen.getByText('Billy: 1')).toBeInTheDocument();
});

test('renders high score of 2 attempts', () => {
  render(<App />);

  userEvent.type(screen.getByLabelText('Your name'), 'Anthony');

  userEvent.type(screen.getByLabelText('Guess'), '42');
  userEvent.click(screen.getByText('Submit'));

  userEvent.clear(screen.getByLabelText('Guess'));

  userEvent.type(screen.getByLabelText('Guess'), '3');
  userEvent.click(screen.getByText('Submit'));

  expect(screen.getByText(/Correct!/i)).toBeInTheDocument();

  expect(screen.getByText('Anthony: 2')).toBeInTheDocument();
});

test('high score should preserve name', () => {
  render(<App />);

  userEvent.type(screen.getByLabelText('Your name'), 'Anthony');
  userEvent.type(screen.getByLabelText('Guess'), '3');
  userEvent.click(screen.getByText('Submit'));

  userEvent.clear(screen.getByLabelText('Your name'));
  userEvent.clear(screen.getByLabelText('Guess'));

  userEvent.type(screen.getByLabelText('Your name'), 'Billy');
  userEvent.type(screen.getByLabelText('Guess'), '3');
  userEvent.click(screen.getByText('Submit'));

  expect(screen.getByText('Anthony: 1')).toBeInTheDocument();
  expect(screen.getByText('Billy: 1')).toBeInTheDocument();
});

