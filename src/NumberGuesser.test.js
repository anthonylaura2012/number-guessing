import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import { NumberGuesser } from "./NumberGuesser";

test('renders initial message', () => {
  render(<NumberGuesser numberPicked={3} setNumTriesRequired={() => {}} />);
  const initialMessage = screen.getByText(/Pick a number between 1 and 10/i);
  expect(initialMessage).toBeInTheDocument();
});

test('can enter correct number', () => {
  render(<NumberGuesser numberPicked={3} setNumTriesRequired={() => {}} />);

  const input = screen.getByLabelText('Guess');
  userEvent.type(input, '3');
  userEvent.click(screen.getByText('Submit'));

  const successMessage = screen.getByText(/Correct!/i);
  expect(successMessage).toBeInTheDocument();
});

test('only show success message when successful', () => {
  render(<NumberGuesser numberPicked={3} setNumTriesRequired={() => {}} />);

  const input = screen.getByLabelText('Guess');
  userEvent.type(input, '3');
  userEvent.click(screen.getByText('Submit'));

  expect(screen.getByText(/Correct!/i)).toBeInTheDocument();
});

test('show failure message', () => {
  render(<NumberGuesser numberPicked={3} setNumTriesRequired={() => {}} />);

  const input = screen.getByLabelText('Guess');
  userEvent.type(input, '2');
  userEvent.click(screen.getByText('Submit'));

  const failureMessage = screen.getByText('Try again :(');
  expect(failureMessage).toBeInTheDocument();
});

test('message starts empty', () => {
  render(<NumberGuesser numberPicked={3} setNumTriesRequired={() => {}} />);

  const successMessage = screen.queryByText('Correct!');
  expect(successMessage).not.toBeInTheDocument();

  const failureMessage = screen.queryByText('Try again :(');
  expect(failureMessage).not.toBeInTheDocument();
});

test('can pass different number as numberPicked', () => {
  render(<NumberGuesser numberPicked={42} setNumTriesRequired={() => {}} />);

  const input = screen.getByLabelText('Guess');
  userEvent.type(input, '42');
  userEvent.click(screen.getByText('Submit'));

  expect(screen.getByText(/Correct!/i)).toBeInTheDocument();
});

test('calls prop setNumTriesRequired when successful', (done) => {
  const fake = function() {
    done();
  };

  render(<NumberGuesser numberPicked={3} setNumTriesRequired={fake}/>);

  const input = screen.getByLabelText('Guess');
  userEvent.type(input, '3');
  userEvent.click(screen.getByText('Submit'));
}, 500);

test('calls prop setNumTriesRequired after first try', (done) => {
  const fake = function(numTries) {
    expect(numTries).toEqual(1);
    done();
  };

  render(<NumberGuesser numberPicked={3} setNumTriesRequired={fake}/>);

  const input = screen.getByLabelText('Guess');
  userEvent.type(input, '3');
  userEvent.click(screen.getByText('Submit'));
}, 500);
