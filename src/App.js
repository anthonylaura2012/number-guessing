import React, { useState } from "react";

import { NumberGuesser } from "./NumberGuesser";
import { HighScores } from "./HighScores";
import { sortFromLowToHigh } from "./sortFromLowToHigh";
import "./App.css";

function App() {
  const [highScores, setHighScores] = useState([]);
  const [yourName, setYourName] = useState();

  const userWon = function(numAttempts) {
    const highScore = {
      name: yourName,
      numTries: numAttempts
    };
    setHighScores([...highScores, highScore]);
  }

  const nameChanged = (e) => setYourName(e.target.value);

  return (
    <div className="App">
      <header className="App-header">
        <label>Your name
          <input type="text" onChange={nameChanged}/>
        </label>
        <NumberGuesser numberPicked={3} setNumTriesRequired={userWon}/>
        <HighScores highScores={sortFromLowToHigh(highScores)} />
      </header>
    </div>
  );
}

export default App;
