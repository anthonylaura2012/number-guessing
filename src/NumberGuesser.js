import React, { useState } from "react";

export function NumberGuesser ({ numberPicked, setNumTriesRequired }) {
  const [value, setValue] = useState(0);
  const [guess, setGuess] = useState(undefined);
  const [numTries, setNumTries] = useState(0);

  const getMessage = () => {
    switch (guess) {
      case numberPicked:
        return "Correct!";
      case undefined:
        return "";
      default:
        return "Try again :(";
    }
  };

  const buttonClicked = (event) => {
    event.preventDefault();

    const totalNumTries = numTries+1;

    const currentGuess = Number(value);
    if (currentGuess === numberPicked) {
      setNumTriesRequired(totalNumTries);
      setNumTries(0);
    } else {
      setNumTries(totalNumTries);
    }

    setGuess(currentGuess);
  };

  const inputChanged = (event) => {
    setValue(event.target.value);
    setGuess(undefined);
  };

  return (
    <>
      <form>
        <p>Pick a number between 1 and 10</p>
        <label>Guess
          <input type="number" name="guess" onChange={inputChanged}/>
        </label>
        <br/>
        <button name="submit" type="submit" onClick={buttonClicked}>
          Submit
        </button>
      </form>
      <p>{getMessage()}</p>
    </>
  );
}